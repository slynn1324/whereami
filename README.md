# whereami

Python terminal script to print IP-Geolocation information from freegeoip.net.

## Installing

```
cp whereami /usr/local/bin/whereami
```